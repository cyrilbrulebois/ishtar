.. Ishtar documentation master file, created by
   sphinx-quickstart on Wed Oct 12 01:01:02 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ishtar's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 3

   installation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

