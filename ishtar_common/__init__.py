#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2014-2015  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

from django.utils.translation import ugettext as _

# overload of translation of registration module
_(u"username")
_(u"email address")
_(u"Related item")
