#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.urlresolvers import reverse
from django.template import Library
from django.utils.translation import ugettext as _

register = Library()


@register.filter
def link_to_window(item):
    return u' <a class="display_details" href="#" '\
        'onclick="load_window(\'{}\')">{}</a>'.format(
            reverse(item.SHOW_URL, args=[item.pk, '']),
            _("Details"))


@register.filter
def link_to_odt(item):
    return reverse(item.SHOW_URL, args=[item.pk, 'odt'])


@register.filter
def link_to_pdf(item):
    return reverse(item.SHOW_URL, args=[item.pk, 'pdf'])
