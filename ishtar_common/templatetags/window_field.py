from django import template
from django.utils.translation import ugettext_lazy as _

register = template.Library()


@register.inclusion_tag('ishtar/blocks/window_field.html')
def field(caption, data, pre_data='', post_data=''):
    if data in (True, False):
        data = _(unicode(data))
    return {'caption': caption, 'data': data, "pre_data": pre_data,
            'post_data': post_data}


@register.inclusion_tag('ishtar/blocks/window_field_url.html')
def field_url(caption, link, link_name=''):
    if not link:
        return u''
    if not link.startswith('http://'):
        link = 'http://' + link
    return {'caption': caption, 'link': link, "link_name": link_name}


@register.inclusion_tag('ishtar/blocks/window_field_multiple.html')
def field_multiple(caption, data):
    return {'caption': caption, 'data': data}
