#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library

register = Library()


@register.inclusion_tag('blocks/form_snippet.html')
def table_form(form):
    return {'form': form}
