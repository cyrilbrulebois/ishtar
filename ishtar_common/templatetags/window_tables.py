import json
import time

from django import template
from django.conf import settings
from django.core.urlresolvers import resolve
from django.template.defaultfilters import slugify
from django.template.loader import get_template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from ishtar_common.forms import reverse_lazy
from ishtar_common.widgets import JQueryJqGrid

from archaeological_files.models import File
from archaeological_operations.models import OperationSource, Operation
from archaeological_context_records.models import ContextRecord, \
    ContextRecordSource
from archaeological_finds.models import Find, FindSource

register = template.Library()


@register.inclusion_tag('ishtar/blocks/window_tables/documents.html')
def table_document(caption, data):
    return {'caption': caption, 'data': data}

ASSOCIATED_MODELS = {}
ASSOCIATED_MODELS['files'] = (File, 'get-file', '')
ASSOCIATED_MODELS['operation_docs'] = (OperationSource,
                                       'get-operationsource', '')
ASSOCIATED_MODELS['operations'] = (Operation, 'get-operation', '')
ASSOCIATED_MODELS['context_records'] = (ContextRecord, 'get-contextrecord',
                                        'get-contextrecord-full')
ASSOCIATED_MODELS['context_records_for_ope'] = (
    ContextRecord,
    'get-contextrecord-for-ope', 'get-contextrecord-full')
ASSOCIATED_MODELS['context_records_docs'] = (ContextRecordSource,
                                             'get-contextrecordsource', '')
ASSOCIATED_MODELS['finds'] = (Find, 'get-find', 'get-find-full')
ASSOCIATED_MODELS['finds_for_ope'] = (
    Find, 'get-find-for-ope', 'get-find-full')
ASSOCIATED_MODELS['finds_docs'] = (FindSource, 'get-findsource', '')


@register.simple_tag(takes_context=True)
def dynamic_table_document(context, caption, associated_model, key, value,
                           table_cols='TABLE_COLS', output='html'):
    if not table_cols:
        table_cols = 'TABLE_COLS'
    model, url, url_full = ASSOCIATED_MODELS[associated_model]
    grid = JQueryJqGrid(None, None, model, table_cols=table_cols)
    source = unicode(reverse_lazy(url))
    source_full = unicode(reverse_lazy(url_full)) if url_full else ''
    source_attrs = mark_safe('?submited=1&{}={}'.format(key, value))
    if output == 'html':
        col_names, extra_cols = grid.get_cols()
        t = get_template('ishtar/blocks/window_tables/dynamic_documents.html')
        context = template.Context({
            'caption': caption,
            'name': slugify(caption) + '{}'.format(int(time.time())),
            'source': source + source_attrs,
            'source_full': source_full,
            'simple_source': source,
            'source_attrs': source_attrs,
            'col_names': col_names,
            'extra_cols': extra_cols,
            'no_result': unicode(_("No results")),
            'loading': unicode(_("Loading...")),
            'encoding': settings.ENCODING or 'utf-8',
        })
        return t.render(context)
    else:
        col_names, extra_cols = grid.get_cols(python=True)
        view, args, kwargs = resolve(source)
        request = context['request']
        if source_attrs and source_attrs.startswith('?'):
            source_attrs = source_attrs[1:]
            dct = {}
            for attr in source_attrs.split('&'):
                if '=' in attr:
                    key, val = attr.split('=')
                    dct[key] = val
            request.GET = dct
        kwargs['request'] = request
        page = view(*args, **kwargs)
        data = []
        if page.content:
            res = json.loads(page.content)
            if "rows" in res:
                for r in res["rows"]:
                    d = []
                    for col in extra_cols:
                        if col in r:
                            d.append(r[col])
                        else:
                            d.append('')
                    data.append(d)
        t = get_template('ishtar/blocks/window_tables/static_documents.html')
        context = template.Context({
            'caption': caption,
            'col_names': col_names,
            'data': data
        })
        return t.render(context)
