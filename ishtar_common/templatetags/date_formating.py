#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from django.template import Library
from django.utils.translation import ugettext as _

register = Library()

@register.filter
def date_formating(value):
    try:
        d = datetime.strptime(unicode(value), '%Y-%m-%d')
        return _(d.strftime("%B")).capitalize() + u" %d" % d.year
    except ValueError:
        # could be passed to non date value: on error return value
        return value

