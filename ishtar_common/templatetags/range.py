#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.template import Library

register = Library()

@register.filter
def get_range(value):
    return [val+1 for val in xrange(value)]
