#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError

from ishtar_common import models


class Command(BaseCommand):
    help = "./manage.py ishtar_import <command> <import_id>\n\n"\
           "Launch the importation a configured import.\n"\
           "<command> must be: \"analyse\", \"import\" or \"archive\"."

    def handle(self, *args, **options):
        if not args or len(args) < 2:
            raise CommandError("<command> and <import_id> are mandatory")
        command = args[0]
        if args[0] not in ["analyse", "import", "archive"]:
            raise CommandError(
                "<command> must be: \"analyse\", \"import\" or \"archive\"."
            )
        try:
            imp = models.Import.objects.get(pk=args[1])
        except (ValueError, models.Import.DoesNotExist):
            raise CommandError("{} is not a valid import ID".format(args[0]))
        if command == 'analyse':
            imp.initialize()
            self.stdout.write("* {} analysed\n".format(imp))
            self.stdout.flush()
        elif command == 'import':
            self.stdout.write("* import {}\n".format(imp))
            imp.importation()
            self.stdout.write("* {} imported\n".format(imp))
            self.stdout.flush()
        elif command == 'archive':
            imp.archive()
            self.stdout.write("*{} archived\n".format(imp))
            self.stdout.flush()
