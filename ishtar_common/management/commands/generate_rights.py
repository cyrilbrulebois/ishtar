#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2011  Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

import ishtar_base.forms_main as ishtar_forms
import ishtar_base.models as models

class Command(BaseCommand):
    args = ''
    help = 'Regenerate rights for current forms'

    def handle(self, *args, **options):
        wizards = []
        wizard_steps = {}
        for attr in dir(ishtar_forms):
            if not attr.endswith('_wizard'):
                continue
            wizard = getattr(ishtar_forms, attr)
            url_name = wizard.url_name
            try:
                wizard_obj = models.Wizard.objects.get(url_name=url_name)
            except ObjectDoesNotExist:
                wizard_obj = models.Wizard.objects.create(url_name=url_name)
                wizard_obj.save()
                #self.stdout.write('* Wizard "%s" added\n' % url_name)
                sys.stdout.write('* Wizard "%s" added\n' % url_name)
            wizard_steps[url_name] = []
            for idx, step_url_name in enumerate(wizard.form_list.keys()):
                form = wizard.form_list[step_url_name]
                if issubclass(form, ishtar_forms.FinalForm):
                    break # don't reference the final form
                step_values = {'name':unicode(form.form_label),
                               'order':idx}
                try:
                    step_obj = models.WizardStep.objects.get(wizard=wizard_obj,
                                                         url_name=step_url_name)
                    for k in step_values:
                        setattr(step_obj, k, step_values[k])
                    step_obj.save()
                except ObjectDoesNotExist:
                    step_values.update({'wizard':wizard_obj,
                                        'url_name':step_url_name})
                    step_obj = models.WizardStep.objects.create(**step_values)
                    step_obj.save()
                    #self.stdout.write('* Wizard step "%s" added\n' \
                    #                  % unicode(form.form_label))
                    sys.stdout.write('* Wizard step "%s" added\n' \
                                      % unicode(form.form_label))
                wizard_steps[url_name].append(step_url_name)
        #self.stdout.write('Successfully regeneration of wizard rights\n')
        sys.stdout.write('Successfully regeneration of wizard rights\n')
