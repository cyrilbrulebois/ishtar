#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import datetime, time

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from ishtar_common import tasks

class Command(BaseCommand):
    help = "Load french towns"

    def handle(self, *args, **options):
        self.stdout.write("* Loading towns\n")
        self.stdout.flush()
        created, updated = tasks.load_towns()
        self.stdout.write("%d towns created, %s towns updated\n\n" % (created,
                                                                      updated))
        self.stdout.flush()

