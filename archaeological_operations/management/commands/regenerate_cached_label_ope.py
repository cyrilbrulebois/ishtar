#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2014 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.db import models
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from archaeological_operations import models

class Command(BaseCommand):
    args = ''
    help = 'Regenerate cached label'

    def handle(self, *args, **options):
        q = models.Operation.objects
        ln = q.count()
        sys.stdout.write("\n* regeneration\n")
        for idx, operation in enumerate(q.all()):
            operation.save()
            sys.stdout.write("\r* %d/%d" % (idx, ln))
