#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2012-2013  Étienne Loks <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

from django.core.management.base import BaseCommand, CommandError
from archaeological_operations.import_from_csv import import_from_csv
from archaeological_operations.import_from_dbf import import_from_dbf

IMPORTERS = {'csv':import_from_csv,
             'dbf':import_from_dbf,
             'db3':import_from_dbf,
             'fp':import_from_dbf,
             'vfp':import_from_dbf}

class Command(BaseCommand):
    args = '<filename> [<lines>]'
    help = "Import archaelogical operations"

    def handle(self, *args, **options):
        if not args or not args[0]:
            raise CommandError("No file provided.")
        filename = args[0]
        update = True
        file_type = None
        lines = len(args) > 1 and args[1]
        if not file_type:
            suffix = filename.split('.')[-1].lower()
            if suffix in IMPORTERS.keys():
                file_type = suffix
            else:
                raise CommandError("This file extension is not managed. "\
                                    "Specify manualy the file type.")
        elif file_type not in IMPORTERS.keys():
            raise CommandError("This file type is not managed.")
        nb_ops, errors = IMPORTERS[file_type](filename,
                                              update=update,
                                              stdout=self.stdout,
                                              lines=lines)
        self.stdout.write('\n* %d operation treated\n' % nb_ops)
        if errors:
            self.stderr.write('\n'.join(errors))
