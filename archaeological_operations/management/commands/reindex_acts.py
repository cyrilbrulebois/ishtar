#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2014 Étienne Loks  <etienne.loks_AT_peacefrogsDOTnet>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# See the file COPYING for details.

import sys

from django.db import models
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from archaeological_operations import models

class Command(BaseCommand):
    args = ''
    help = 'Regenerate index'

    def handle(self, *args, **options):
        # first handle not appropriate index
        q = models.AdministrativeAct.objects.filter(
                                    signature_date__isnull=False,
                                    index__isnull=False
                                    ).exclude(act_type__indexed=True)
        ln = q.count()
        sys.stdout.write("\n* clean\n")
        for idx, admin_act in enumerate(q.all()):
            admin_act.index = None
            admin_act.save()
            sys.stdout.write("\r* %d/%d" % (idx, ln))
        # then try to fix
        q = models.AdministrativeAct.objects
        ln = q.count()
        sys.stdout.write("\n* fix\n")
        tobefix = []
        for idx, admin_act in enumerate(q.all()):
            sys.stdout.write("\r* %d/%d" % (idx, ln))
            try:
                admin_act.save()
            except ValidationError:
                tobefix.append(admin_act)
        print "To be fix"
        for item in tobefix:
            print u";".join([unicode(v) for v in (item.signature_date.year,
                                    item.index, item.act_type,
                                    item.operation or item.associated_file)])
