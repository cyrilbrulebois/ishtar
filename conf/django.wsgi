import os, sys
sys.path.append('/srv/ishtar/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'ishtar.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
