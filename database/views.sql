create view file_department (id, department_id, file_id) as
    select town."id", town."departement_id", file_towns."file_id"
        from ishtar_base_town town
        inner join ishtar_base_file_towns file_towns on
        file_towns."town_id"=town."id" order by town."departement_id";
CREATE RULE file_department_delete
    AS ON DELETE TO file_department DO INSTEAD();
create view operation_department (id, department_id, operation_id) as
    select town."id", town."departement_id", operation_towns."operation_id"
       from ishtar_base_town town
       inner join ishtar_base_operation_towns operation_towns on
       operation_towns."town_id"=town."id" order by town."departement_id";
CREATE RULE operation_department_delete
       AS ON DELETE TO operation_department DO INSTEAD();

