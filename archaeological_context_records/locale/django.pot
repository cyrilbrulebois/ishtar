# Ishtar po translation.
# Copyright (C) 2010-2015
# This file is distributed under the same license as the Ishtar package.
# Étienne Loks <etienne.loks at peacefrogs net>, 2010-2015.
# Valérie-Emma Leroux <emma@iggdrasil.net>, 2016. #zanata
msgid ""
msgstr ""

#: forms.py:45 forms.py:104 models.py:125
#: templates/ishtar/sheet_contextrecord.html:72
msgid "ID"
msgstr ""

#: forms.py:49
msgid "Code PATRIARCHE"
msgstr ""

#: forms.py:50
msgid "Operation's year"
msgstr ""

#: forms.py:52
msgid "Operation's number (index by year)"
msgstr ""

#: forms.py:54
msgid "Archaelogical site"
msgstr ""

#: forms.py:59 forms.py:170 models.py:48
msgid "Period"
msgstr ""

#: forms.py:60
msgid "Unit type"
msgstr ""

#: forms.py:61
msgid "Parcel (section/number)"
msgstr ""

#: forms.py:79 forms.py:266 views.py:78
msgid "Context record search"
msgstr ""

#: forms.py:93
msgid "You should at least select one context record."
msgstr ""

#: forms.py:99
msgid "General"
msgstr ""

#: forms.py:103 models.py:118 models.py:121
#: templates/ishtar/sheet_contextrecord.html:76
msgid "Parcel"
msgstr ""

#: forms.py:106 models.py:126 templates/ishtar/sheet_contextrecord.html:42
#: templates/ishtar/sheet_contextrecord.html:75
msgid "Description"
msgstr ""

#: forms.py:108 models.py:131
msgid "Length (cm)"
msgstr ""

#: forms.py:109 models.py:132
msgid "Width (cm)"
msgstr ""

#: forms.py:110 models.py:133
msgid "Thickness (cm)"
msgstr ""

#: forms.py:111 models.py:135
msgid "Depth (cm)"
msgstr ""

#: forms.py:112 models.py:141
msgid "Unit"
msgstr ""

#: forms.py:114 models.py:137
msgid "Location"
msgstr ""

#: forms.py:159
msgid "This ID already exists for this operation."
msgstr ""

#: forms.py:165 forms.py:189 models.py:59
msgid "Dating"
msgstr ""

#: forms.py:171 models.py:49
msgid "Start date"
msgstr ""

#: forms.py:172 models.py:50 models.py:130
msgid "End date"
msgstr ""

#: forms.py:173 models.py:53
msgid "Quality"
msgstr ""

#: forms.py:174 models.py:35 models.py:51
msgid "Dating type"
msgstr ""

#: forms.py:198 ishtar_menu.py:29 models.py:321
msgid "Context record"
msgstr ""

#: forms.py:218
msgid "Relations"
msgstr ""

#: forms.py:222 forms.py:229 models.py:146
#: templates/ishtar/sheet_contextrecord.html:52
msgid "Interpretation"
msgstr ""

#: forms.py:225 models.py:143
msgid "Has furniture?"
msgstr ""

#: forms.py:227 models.py:145
msgid "Filling"
msgstr ""

#: forms.py:231 models.py:166
msgid "Activity"
msgstr ""

#: forms.py:233 models.py:164
msgid "Identification"
msgstr ""

#: forms.py:235 models.py:149
msgid "TAQ"
msgstr ""

#: forms.py:236 models.py:153
msgid "Estimated TAQ"
msgstr ""

#: forms.py:238 models.py:156
msgid "TPQ"
msgstr ""

#: forms.py:239 models.py:160
msgid "Estimated TPQ"
msgstr ""

#: forms.py:252
msgid "Operation search"
msgstr ""

#: forms.py:254
msgid "You should select an operation."
msgstr ""

#: forms.py:259
msgid "Would you like to delete this context record?"
msgstr ""

#: forms.py:268
msgid "You should select a context record."
msgstr ""

#: forms.py:273
msgid "Town of the operation"
msgstr ""

#: forms.py:275
msgid "Year of the operation"
msgstr ""

#: forms.py:277
msgid "Period of the context record"
msgstr ""

#: forms.py:279
msgid "Unit type of the context record"
msgstr ""

#: forms.py:292
msgid "Documentation search"
msgstr ""

#: forms.py:294
msgid "You should select a document."
msgstr ""

#: ishtar_menu.py:31
msgid "Search"
msgstr ""

#: ishtar_menu.py:35
msgid "Creation"
msgstr ""

#: ishtar_menu.py:39 ishtar_menu.py:56
msgid "Modification"
msgstr ""

#: ishtar_menu.py:43 ishtar_menu.py:62
msgid "Deletion"
msgstr ""

#: ishtar_menu.py:47
msgid "Documentation"
msgstr ""

#: ishtar_menu.py:50
msgid "Add"
msgstr ""

#: models.py:36
msgid "Dating types"
msgstr ""

#: models.py:42
msgid "Dating quality"
msgstr ""

#: models.py:43
msgid "Dating qualities"
msgstr ""

#: models.py:55
msgid "Precise dating"
msgstr ""

#: models.py:60
msgid "Datings"
msgstr ""

#: models.py:71 models.py:85 models.py:97
msgid "Order"
msgstr ""

#: models.py:72
msgid "Parent unit"
msgstr ""

#: models.py:76
msgid "Unit Type"
msgstr ""

#: models.py:77
msgid "Unit Types"
msgstr ""

#: models.py:88
msgid "Activity Type"
msgstr ""

#: models.py:89
msgid "Activity Types"
msgstr ""

#: models.py:100
msgid "Identification Type"
msgstr ""

#: models.py:101
msgid "Identification Types"
msgstr ""

#: models.py:119
msgid "External ID"
msgstr ""

#: models.py:123 wizards.py:68
msgid "Operation"
msgstr ""

#: models.py:127
msgid "Comment"
msgstr ""

#: models.py:128
msgid "Date d'ouverture"
msgstr ""

#: models.py:138
msgid "A short description of the location of the context record"
msgstr ""

#: models.py:150
msgid ""
"\"Terminus Ante Quem\" the context record can't have been created after this "
"date"
msgstr ""

#: models.py:154
msgid "Estimation of a \"Terminus Ante Quem\""
msgstr ""

#: models.py:157
msgid ""
"\"Terminus Post Quem\" the context record can't have been created before "
"this date"
msgstr ""

#: models.py:161
msgid "Estimation of a \"Terminus Post Quem\""
msgstr ""

#: models.py:172 models.py:173 templates/ishtar/sheet_contextrecord.html:6
msgid "Context Record"
msgstr ""

#: models.py:175
msgid "Can view all Context Records"
msgstr ""

#: models.py:177
msgid "Can view own Context Record"
msgstr ""

#: models.py:179
msgid "Can add own Context Record"
msgstr ""

#: models.py:181
msgid "Can change own Context Record"
msgstr ""

#: models.py:183
msgid "Can delete own Context Record"
msgstr ""

#: models.py:192
msgctxt "short"
msgid "Context record"
msgstr ""

#: models.py:290
msgid "Inverse relation"
msgstr ""

#: models.py:294 templates/ishtar/sheet_contextrecord.html:71
msgid "Relation type"
msgstr ""

#: models.py:295
msgid "Relation types"
msgstr ""

#: models.py:308
msgid "Record relation"
msgstr ""

#: models.py:309
msgid "Record relations"
msgstr ""

#: models.py:318
msgid "Context record documentation"
msgstr ""

#: models.py:319
msgid "Context record documentations"
msgstr ""

#: views.py:88
msgid "New context record"
msgstr ""

#: views.py:98
msgid "Context record modification"
msgstr ""

#: views.py:112
msgid "Context record deletion"
msgstr ""

#: views.py:120
msgid "Context record: new source"
msgstr ""

#: views.py:128
msgid "Context record: source modification"
msgstr ""

#: views.py:134
msgid "Context record: source deletion"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:13
msgid "Previous version"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:17
msgid "Are you sure to rollback to this version?"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:18
msgid "Next version"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:22
msgid "Export as:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:22
msgid "OpenOffice.org file"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:22
msgid "PDF file"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:23
msgid "Modify"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:26
msgid "Complete ID:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:28
#: templates/ishtar/sheet_contextrecord.html:101
msgid "Patriarche OA code not yet recorded!"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:29
msgid "Temporary ID:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:32
msgid "Creation date:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:33
msgid "Created by:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:35
#: templates/ishtar/sheet_contextrecord.html:114
msgid "Type:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:37
msgid "Chronology:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:38
msgid "Place:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:39
msgid "Parcel:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:44
msgid "Description:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:45
#: templates/ishtar/sheet_contextrecord.html:117
msgid "Comment:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:46
msgid "Length (cm):"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:47
msgid "Width (cm):"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:48
msgid "Depth (cm):"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:54
msgid "Activity:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:55
msgid "Identification:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:56
msgid "Interpretation:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:60
msgid "Datations"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:61
msgid "TAQ:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:62
msgid "Estimated TAQ:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:63
msgid "TPQ:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:64
msgid "Estimated TPQ:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:68
msgid "In relation with"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:73
msgid "Type"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:74
msgid "Chronology"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:87
msgid "Details"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:95
msgid "Operation summary"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:96
msgid "Year:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:99
msgid "Patriarche OA code:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:104
msgid "Head scientist:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:106
msgid "State:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:108
msgid "Active file"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:110
msgid "Closed operation"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:111
msgid "Closing date:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:112
msgid "by"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:115
msgid "Remains:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:116
msgid "Periods:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:118
msgid "Localisation"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:119
msgid "Towns:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:120
msgid "Related operation:"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:123
msgid "No operation linked to this context unit!"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:126
msgid "Document from this context record"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:131
msgid "Finds"
msgstr ""

#: templates/ishtar/sheet_contextrecord.html:136
msgid "Documents from associated finds"
msgstr ""

#: templates/ishtar/sheet_contextrecordsource.html:6
msgid "Context record source"
msgstr ""
