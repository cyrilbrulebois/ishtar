# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DatingType'
        db.create_table('archaeological_context_records_datingtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('txt_idx', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('available', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('archaeological_context_records', ['DatingType'])

        # Adding model 'DatingQuality'
        db.create_table('archaeological_context_records_datingquality', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('txt_idx', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('available', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('archaeological_context_records', ['DatingQuality'])

        # Adding model 'Dating'
        db.create_table('archaeological_context_records_dating', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('period', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['archaeological_operations.Period'])),
            ('start_date', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('dating_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['archaeological_context_records.DatingType'], null=True, blank=True)),
            ('quality', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['archaeological_context_records.DatingQuality'], null=True, blank=True)),
        ))
        db.send_create_signal('archaeological_context_records', ['Dating'])

        # Adding model 'Unit'
        db.create_table('archaeological_context_records_unit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('txt_idx', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')()),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['archaeological_context_records.Unit'], null=True, blank=True)),
        ))
        db.send_create_signal('archaeological_context_records', ['Unit'])

        # Adding model 'ActivityType'
        db.create_table('archaeological_context_records_activitytype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('txt_idx', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('archaeological_context_records', ['ActivityType'])

        # Adding model 'IdentificationType'
        db.create_table('archaeological_context_records_identificationtype', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('txt_idx', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('comment', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('available', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('order', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('archaeological_context_records', ['IdentificationType'])

        # Adding model 'HistoricalContextRecord'
        db.create_table('archaeological_context_records_historicalcontextrecord', (
            ('id', self.gf('django.db.models.fields.IntegerField')(db_index=True, blank=True)),
            ('history_modifier_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('parcel_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('operation_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('length', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('width', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('thickness', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('depth', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('unit_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('has_furniture', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('filling', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('interpretation', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('taq', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('taq_estimated', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tpq', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tpq_estimated', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('identification_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('activity_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('history_id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('history_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('history_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True)),
            ('history_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal('archaeological_context_records', ['HistoricalContextRecord'])

        # Adding model 'ContextRecord'
        db.create_table('archaeological_context_records_contextrecord', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('history_modifier', self.gf('django.db.models.fields.related.ForeignKey')(related_name='+', to=orm['auth.User'])),
            ('parcel', self.gf('django.db.models.fields.related.ForeignKey')(related_name='context_record', to=orm['archaeological_operations.Parcel'])),
            ('operation', self.gf('django.db.models.fields.related.ForeignKey')(related_name='context_record', to=orm['archaeological_operations.Operation'])),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('length', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('width', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('thickness', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('depth', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('unit', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['archaeological_context_records.Unit'])),
            ('has_furniture', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('filling', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('interpretation', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('taq', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('taq_estimated', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tpq', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('tpq_estimated', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('identification', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['archaeological_context_records.IdentificationType'], null=True, blank=True)),
            ('activity', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['archaeological_context_records.ActivityType'], null=True, blank=True)),
        ))
        db.send_create_signal('archaeological_context_records', ['ContextRecord'])

        # Adding M2M table for field datings on 'ContextRecord'
        db.create_table('archaeological_context_records_contextrecord_datings', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contextrecord', models.ForeignKey(orm['archaeological_context_records.contextrecord'], null=False)),
            ('dating', models.ForeignKey(orm['archaeological_context_records.dating'], null=False))
        ))
        db.create_unique('archaeological_context_records_contextrecord_datings', ['contextrecord_id', 'dating_id'])

        # Adding model 'ContextRecordSource'
        db.create_table('archaeological_context_records_contextrecordsource', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('source_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ishtar_common.SourceType'])),
            ('associated_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('receipt_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('creation_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('context_record', self.gf('django.db.models.fields.related.ForeignKey')(related_name='source', to=orm['archaeological_context_records.ContextRecord'])),
        ))
        db.send_create_signal('archaeological_context_records', ['ContextRecordSource'])

        # Adding M2M table for field authors on 'ContextRecordSource'
        db.create_table('archaeological_context_records_contextrecordsource_authors', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contextrecordsource', models.ForeignKey(orm['archaeological_context_records.contextrecordsource'], null=False)),
            ('author', models.ForeignKey(orm['ishtar_common.author'], null=False))
        ))
        db.create_unique('archaeological_context_records_contextrecordsource_authors', ['contextrecordsource_id', 'author_id'])


    def backwards(self, orm):
        # Deleting model 'DatingType'
        db.delete_table('archaeological_context_records_datingtype')

        # Deleting model 'DatingQuality'
        db.delete_table('archaeological_context_records_datingquality')

        # Deleting model 'Dating'
        db.delete_table('archaeological_context_records_dating')

        # Deleting model 'Unit'
        db.delete_table('archaeological_context_records_unit')

        # Deleting model 'ActivityType'
        db.delete_table('archaeological_context_records_activitytype')

        # Deleting model 'IdentificationType'
        db.delete_table('archaeological_context_records_identificationtype')

        # Deleting model 'HistoricalContextRecord'
        db.delete_table('archaeological_context_records_historicalcontextrecord')

        # Deleting model 'ContextRecord'
        db.delete_table('archaeological_context_records_contextrecord')

        # Removing M2M table for field datings on 'ContextRecord'
        db.delete_table('archaeological_context_records_contextrecord_datings')

        # Deleting model 'ContextRecordSource'
        db.delete_table('archaeological_context_records_contextrecordsource')

        # Removing M2M table for field authors on 'ContextRecordSource'
        db.delete_table('archaeological_context_records_contextrecordsource_authors')


    models = {
        'archaeological_context_records.activitytype': {
            'Meta': {'object_name': 'ActivityType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_context_records.contextrecord': {
            'Meta': {'object_name': 'ContextRecord'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_context_records.ActivityType']", 'null': 'True', 'blank': 'True'}),
            'datings': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['archaeological_context_records.Dating']", 'symmetrical': 'False'}),
            'depth': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'filling': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'has_furniture': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'history_modifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identification': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_context_records.IdentificationType']", 'null': 'True', 'blank': 'True'}),
            'interpretation': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'length': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'operation': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'context_record'", 'to': "orm['archaeological_operations.Operation']"}),
            'parcel': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'context_record'", 'to': "orm['archaeological_operations.Parcel']"}),
            'taq': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'taq_estimated': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thickness': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tpq': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tpq_estimated': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'unit': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['archaeological_context_records.Unit']"}),
            'width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'archaeological_context_records.contextrecordsource': {
            'Meta': {'object_name': 'ContextRecordSource'},
            'associated_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'authors': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['ishtar_common.Author']", 'symmetrical': 'False'}),
            'context_record': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'source'", 'to': "orm['archaeological_context_records.ContextRecord']"}),
            'creation_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'receipt_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'source_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.SourceType']"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'archaeological_context_records.dating': {
            'Meta': {'object_name': 'Dating'},
            'dating_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_context_records.DatingType']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'period': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_operations.Period']"}),
            'quality': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_context_records.DatingQuality']", 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'archaeological_context_records.datingquality': {
            'Meta': {'object_name': 'DatingQuality'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_context_records.datingtype': {
            'Meta': {'object_name': 'DatingType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_context_records.historicalcontextrecord': {
            'Meta': {'ordering': "('-history_date', '-history_id')", 'object_name': 'HistoricalContextRecord'},
            'activity_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'depth': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'filling': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'has_furniture': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'history_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'history_id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'history_modifier_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'history_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'history_user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True'}),
            'id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'blank': 'True'}),
            'identification_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'interpretation': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'length': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'operation_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'parcel_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'taq': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'taq_estimated': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'thickness': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tpq': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'tpq_estimated': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'unit_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'width': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'archaeological_context_records.identificationtype': {
            'Meta': {'object_name': 'IdentificationType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_context_records.unit': {
            'Meta': {'object_name': 'Unit'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_context_records.Unit']", 'null': 'True', 'blank': 'True'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_files.file': {
            'Meta': {'ordering': "['-year', '-numeric_reference']", 'object_name': 'File'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'address_complement': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'file_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_files.FileType']"}),
            'general_contractor': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['ishtar_common.Person']"}),
            'history_modifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_charge': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['ishtar_common.Person']"}),
            'internal_reference': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'numeric_reference': ('django.db.models.fields.IntegerField', [], {}),
            'permit_reference': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'permit_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_files.PermitType']", 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'reception_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'reference_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'related_file': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_files.File']", 'null': 'True', 'blank': 'True'}),
            'saisine_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_files.SaisineType']", 'null': 'True', 'blank': 'True'}),
            'total_developed_surface': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'total_surface': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'town_planning_service': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['ishtar_common.Organization']"}),
            'towns': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'file'", 'symmetrical': 'False', 'to': "orm['ishtar_common.Town']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'default': '2012'})
        },
        'archaeological_files.filetype': {
            'Meta': {'object_name': 'FileType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_files.permittype': {
            'Meta': {'object_name': 'PermitType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_files.saisinetype': {
            'Meta': {'object_name': 'SaisineType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'delay': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_operations.operation': {
            'Meta': {'object_name': 'Operation'},
            'associated_file': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'operations'", 'null': 'True', 'to': "orm['archaeological_files.File']"}),
            'code_dracar': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'code_patriarche': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'common_name': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'cost': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'effective_man_days': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'excavation_end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'fnap_cost': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'fnap_financing': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'geoarchaeological_context_prescription': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'history_modifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_charge': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['ishtar_common.Person']"}),
            'large_area_prescription': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'operation_code': ('django.db.models.fields.IntegerField', [], {}),
            'operation_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['archaeological_operations.OperationType']"}),
            'operator_reference': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'optional_man_days': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'periods': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['archaeological_operations.Period']", 'symmetrical': 'False'}),
            'remains': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['archaeological_operations.RemainType']", 'symmetrical': 'False'}),
            'scheduled_man_days': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'surface': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'towns': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['ishtar_common.Town']", 'symmetrical': 'False'}),
            'year': ('django.db.models.fields.IntegerField', [], {}),
            'zoning_prescription': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        'archaeological_operations.operationtype': {
            'Meta': {'object_name': 'OperationType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_operations.parcel': {
            'Meta': {'object_name': 'Parcel'},
            'associated_file': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'parcels'", 'null': 'True', 'to': "orm['archaeological_files.File']"}),
            'history_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'history_modifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'operation': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'parcels'", 'null': 'True', 'to': "orm['archaeological_operations.Operation']"}),
            'parcel_number': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'section': ('django.db.models.fields.CharField', [], {'max_length': '4'}),
            'town': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'parcels'", 'to': "orm['ishtar_common.Town']"}),
            'year': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'archaeological_operations.period': {
            'Meta': {'object_name': 'Period'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['archaeological_operations.Period']", 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.IntegerField', [], {}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'archaeological_operations.remaintype': {
            'Meta': {'object_name': 'RemainType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'ishtar_common.arrondissement': {
            'Meta': {'object_name': 'Arrondissement'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'ishtar_common.author': {
            'Meta': {'object_name': 'Author'},
            'author_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.AuthorType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'person': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Person']"})
        },
        'ishtar_common.authortype': {
            'Meta': {'object_name': 'AuthorType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'ishtar_common.canton': {
            'Meta': {'object_name': 'Canton'},
            'arrondissement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Arrondissement']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'ishtar_common.department': {
            'Meta': {'ordering': "['number']", 'object_name': 'Department'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'number': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'})
        },
        'ishtar_common.organization': {
            'Meta': {'object_name': 'Organization'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'address_complement': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'history_modifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile_phone': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'organization_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.OrganizationType']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        'ishtar_common.organizationtype': {
            'Meta': {'object_name': 'OrganizationType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'ishtar_common.person': {
            'Meta': {'object_name': 'Person'},
            'address': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'address_complement': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'attached_to': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Organization']", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'history_modifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['auth.User']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mobile_phone': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'person_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.PersonType']"}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '18', 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'town': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'})
        },
        'ishtar_common.persontype': {
            'Meta': {'object_name': 'PersonType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'rights': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['ishtar_common.WizardStep']", 'symmetrical': 'False'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'ishtar_common.sourcetype': {
            'Meta': {'object_name': 'SourceType'},
            'available': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'comment': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'txt_idx': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'ishtar_common.town': {
            'Meta': {'ordering': "['numero_insee']", 'object_name': 'Town'},
            'canton': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Canton']", 'null': 'True', 'blank': 'True'}),
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '27572', 'null': 'True', 'blank': 'True'}),
            'departement': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Department']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'numero_insee': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '6'}),
            'surface': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        'ishtar_common.wizard': {
            'Meta': {'ordering': "['url_name']", 'object_name': 'Wizard'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url_name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        'ishtar_common.wizardstep': {
            'Meta': {'ordering': "['wizard', 'order']", 'object_name': 'WizardStep'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'order': ('django.db.models.fields.IntegerField', [], {}),
            'url_name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'wizard': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['ishtar_common.Wizard']"})
        }
    }

    complete_apps = ['archaeological_context_records']